﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibParking
{
    public enum Kleur { Blauw, Grijs, Groen, Rood }

    public class Auto
    {
        public enum Merk { BMW, Opel, Renault, Toyota}
        

        public string Bestuurder { get; set; }
        public Kleur AutoKleur { get; set; }
        public Merk AutoMerk { get; set; }

        public Auto() { }

        public Auto(string bestuurder, Merk merk, Kleur kleur)
        {
            this.Bestuurder = bestuurder;
            this.AutoMerk = merk;
            this.AutoKleur = kleur;
        }

        public override string ToString()
        {
            // Omschrijving van een Auto
            return Bestuurder + ": " + AutoMerk + " - " + AutoKleur;
        }
    }
}
