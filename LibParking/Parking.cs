﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibParking
{
    public class Parking
    {
        private int maxAantalAutos;
        int MaxAantalAutos {
            get { return maxAantalAutos; }
            set
            {
                // controle of maxAantal groter is dan 0, anders een Exception
                if (value > 0) maxAantalAutos = value;
                else throw new Exception("Het maximum aantal moet groter zijn dan 0");
            } 
        }

        private List<Auto> Autos { get; set; }

        public Parking(): this(10)
        {
            
        }

        public Parking(int maxAantalAutos)
        {
            MaxAantalAutos = maxAantalAutos;
            Autos = new List<Auto>();
        }


        public void NieuweAuto(Auto auto)
        {
            if (Autos.Count < MaxAantalAutos)
            {
                Autos.Add(auto);
            }
            else
            {
                throw new Exception("Parking is vol");
            }
        }

        public List<Auto> GetAutos()
        {
            // Retourneer de lijst van autos
            return Autos;
        }
    }
}
