﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LibParking;

namespace WpfParking
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Parking parking = new Parking(5);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // met een lus de merken toevoegen aan de listbox
            foreach(string merk in  Enum.GetNames(typeof(Auto.Merk)))
            {
                lbMerk.Items.Add(merk);
            }

            // andere manier voor de kleuren: werken met ItemsSource
            lbKleur.ItemsSource = Enum.GetNames(typeof (Kleur));
            lbAutos.ItemsSource = parking.GetAutos();
        }

        private void btnAutoToevoegen_Click(object sender, RoutedEventArgs e)
        {
            // Gegevens voor de Auto inlezen en auto in de parking plaatsen
            string bestuurder = tbBestuurder.Text;
            if (Enum.GetValues(typeof (Auto.Merk)).Length > 0 && Enum.GetValues(typeof (Kleur)).Length > 0)
            {
                if (lbMerk.SelectedIndex < 0) lbMerk.SelectedIndex = 0;
                Auto.Merk merk = (Auto.Merk) Enum.Parse(typeof (Auto.Merk), lbMerk.SelectedItem.ToString());
                if (lbKleur.SelectedIndex < 0) lbKleur.SelectedIndex = 0;
                Kleur kleur = (Kleur) Enum.Parse(typeof (Kleur), lbKleur.SelectedItem.ToString());

                Auto auto = new Auto(bestuurder, merk, kleur);

                try
                {
                    parking.NieuweAuto(auto);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }

                tbBestuurder.Text = "";
                tbBestuurder.Focus();

                ToonAutos();
            }
            else
            {
                MessageBox.Show("Onvoldoende basisgegevens (Merk/Kleur)");
            }
        }

        private void ToonAutos()
        {
            lbAutos.ItemsSource = null;
            lbAutos.ItemsSource = parking.GetAutos();

        }
    }
}
